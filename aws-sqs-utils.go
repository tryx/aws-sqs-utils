package aws_sqs_utils

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
)

func S3EventRecordsFromSQS(event events.SQSEvent) (err error, s3EventRecords []events.S3EventRecord) {

	for _, sqsMessage := range event.Records {
		fmt.Println(sqsMessage)
		var s3Event events.S3Event
		err = json.Unmarshal([]byte(sqsMessage.Body), &s3Event)
		if err != nil {
			panic(err)
		}
		fmt.Println(s3Event)
		for _, s3EventRecord := range s3Event.Records {
			s3EventRecords = append(s3EventRecords, s3EventRecord)
		}
	}
	return
}
